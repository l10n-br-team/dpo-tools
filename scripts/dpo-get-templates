#!/bin/sh
#
#    Copyright (C) 2008 Adriano Rafael Gomes <adrianorg@arg.eti.br>
#    Copyright (C) 2018 Debian Brazilian Localization Team <contato@debianbrasil.org.br>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

if [ -z "$1" ]
then
  echo "The package name or the po file name is missing (use either)\n"
  exit 1
fi

pkg=${1%_pt_BR.po}
url=$(
  wget \
    --quiet \
    "https://www.debian.org/international/l10n/po-debconf/pot" \
    --output-document - |
  grep "\/$pkg\/" |
  grep --only-matching "https://i18n[^\"]*_templates.pot.gz" |
  sed -e 's/%3a/:/'
)
echo "URL='$url'\n"
if [ -z "$url" ]
then
    echo "Template not found\n"
    exit 1
fi
templates_gz=${url##*/}
templates=${templates_gz%.gz}
if [ -f $templates ]
then
  echo "$templates file already exists and updated\n"
  exit 1
fi
wget --quiet --no-clobber $url
gunzip $templates
